import { Response, Request, NextFunction } from "express";
import { buildSkill } from "../util/alexa-handlers";

export let postAlexa = (req: Request, res: Response, next: NextFunction) => {
    //console.log(req, res);

    const skill = buildSkill();
    
    skill.invoke(req.body)
    .then((response) => {
        res.send(response.response);
    });
  };